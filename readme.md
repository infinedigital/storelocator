# Store Locator Custom

[![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)](https://infine.net) 

Code php pour avoir un store locator lié a une GoogleSheet.

## Pour commencer

Il faut une API Google et verifié si l'Api qui fait le lien entre la GoogleSheet et le code soit up.

### Pré-requis

Ce qu'il est requis pour commencer avec votre projet...

- PHP 7.3
- jQuery
- Api Google

### Installation
1- Verifier dans le .env la variable ```WP_THEME```

2- Installation via composer
```
composer require remiinfine/storelocator dev-master
```
3- Faire une copie ```/remiinfine/storelocator/src/assets/js/store.js``` dans ```/resources/scripts/```

4- Faire une copie ```/remiinfine/storelocator/src/assets/scss/storelocator.scss``` dans ```resources/styles/components/```

5- Ajouter ```@import "components/storelocator";``` dans resources/styles.app.scss

6- Ajouter ```mix.copy('./resources/assets/json/store.json', './public/themes/'+theme+'/assets/json/store.json', false);``` dans webpack.mix.js

6- Ajouter ```mix.copy('./resources/assets/scripts/store.js', './public/themes/'+theme+'/assets/scripts/store.js', false);``` dans webpack.mix.js

7- Creer un dossier ```json``` avec un fichier ```store.json``` dans le dossier resource du projet

8- Faire un ```npm run dev```

9- Avoir l'id de la GoogleSheet :

```
https://docs.google.com/spreadsheets/d/1W3eZRqrwiQYQDBg9eMvX7Ktt_2aWcWxCDWhKjG7eVzI/edit#gid=0
```

>Ici c'est : **1W3eZRqrwiQYQDBg9eMvX7Ktt_2aWcWxCDWhKjG7eVzI**

10- Avoir une [ApiKey Google map](https://console.cloud.google.com/).


- Recuper l'url de l'api et la copier dans store.php.

```
http://gsx2json.com/api?id=<IDDANSLURLDELASPREADSHEET>&sheet=1&columns=false
```

11- Dans le HTML:

```html
 <div id="map"></div>
<?php \StoreLocator\Data::isUpdate('http://gsx2json.com/api?id=1PwfpDShjRuxOOe9uqTgWKUblVKoKijaLqkFJ-_8jg4s&sheet=1&columns=false', 'http://gsx2json.com/api?id=1PwfpDShjRuxOOe9uqTgWKUblVKoKijaLqkFJ-_8jg4s&sheet=1&columns=false', 'AIzaSyBLSZN6ndfqZEc31wZQ5KO5mKMzaT-BQ4o'); ?>
<?php $filters = \StoreLocator\Data::listFilter('http://gsx2json.com/api?id=1PwfpDShjRuxOOe9uqTgWKUblVKoKijaLqkFJ-_8jg4s&sheet=2&columns=false','AIzaSyBLSZN6ndfqZEc31wZQ5KO5mKMzaT-BQ4o'); ?>
<script id="jsStore" data-api_key="AIzaSyBLSZN6ndfqZEc31wZQ5KO5mKMzaT-BQ4o" data-theme="<?php echo env('WP_THEME') ?>" src="./themes/<?php echo env('WP_THEME') ?>/assets/scripts/store.js" data-placeholder="<?php echo $storelocplaceholder ?>" data-filter="<?php echo htmlspecialchars(json_encode($filters)) ?>"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7w7AhjjiY6ul0aJ2_rXt0vZRpqiq9cX4&libraries=places&callback=initMap"></script>
<!--            AJOUT IF FILTER ACF ?-->
<div class="mapWrap">
    <div id="map-canvas"></div>
    <div class="investCast">
        <select id="mag" onchange="filterMarkers(this.value);">
            <option value="">Select your product</option>
            <?php
            foreach ($filters as $filter) : ?>
                <option value="<?php echo $filter?>"><?php echo $filter?></option>
            <?php endforeach ?>
        </select>
    </div>
</div>
```

12 - Creation de l'ACF pour edit le placeholder dans ```class-initacfoptions.php```:
```
acf_tab(
    [
        'name'  => 'map_tab',
        'label' => 'Store Locator',
    ]
),
acf_text(
    [
        'name'          => 'map_placeholder',
        'label'         => 'Placeholder map',
        'default_value' => 'Enter an address ',
    ]
),
```
> :warning: Ajouter le get_field sur la page : ***$storelocplaceholder = get_field('map_placeholder', 'options');***



## Fabriqué avec

* [GoogleSheet](https://www.google.com/intl/fr_BE/sheets/about/) - Feuille de calcul
* [Gsx2Json](http://gsx2json.com/) - GoogleSpreadsheet to Json API Service (OpenSource)


## Versions
Listez les versions ici 
**Dernière version stable :** 1.0


## Auteurs
**Rémi Baltus** _alias_ [@remibaltus](https://bitbucket.org/remibaltus/)

## License

©[infine](https://infine.net)
