<?php


namespace StoreLocator;

ini_set('display_errors', 0);

class Data
{
    public static function isUpdate($lastupdate, $url, $apikey)
    {
        $last = json_decode(file_get_contents($lastupdate));
        $lastGoogleUpdate = $last->rows[0]->lastedit;
        $lastJsonUpdate = json_decode(file_get_contents('./themes/'.env('WP_THEME').'/assets/json/store.json'));

        if($lastGoogleUpdate > $lastJsonUpdate->lastupdate )
        {
            self::googleToJson($url, $apikey, $lastGoogleUpdate);
        }
    }

    public static function googleToJson($url, $apikey, $lastGoogleUpdate)
    {
        $json = file_get_contents($url);
        $listestores  = json_decode($json);
        $listfeatures = [];
        foreach ($listestores->rows as $store) {
            $concataddress = $store->street.'+'.$store->zipcode.'+'.$store->city;
            $address = str_replace([':', '\\', '/', '*', ' ', '\''], '+', $concataddress);
            $array = explode(',', $store->products);
            $productsArray = str_replace(' ', '', $array);
            if($store->lng && $store->lat) {
                $features= [
                    "geometry" => [
                        "type" => "Point",
                        "coordinates" => [
                            $store->lng,
                            $store->lat
                        ]
                    ],
                    "type" => "Feature",
                    "properties" => [
                        "category" => "",
                        "hours" => "10am - 6pm",
                        "description" => "Modern twists on classic pastries. We're part of a larger chain of patisseries and cafes.",
                        "name" => $store->name,
                        "adress" => $store->street.' '.$store->zipcode.' '.$store->city,
                        "phone" => $store->phone,
                        "storeid" => $store->storeid,
                        "products"  => $store->products
                    ]
                ];
                $listfeatures[] = $features;
            }
        }
        $jsonstore = [
            "type" => "FeatureCollection",
            "features" =>
                $listfeatures
            ,
            "lastupdate" => $lastGoogleUpdate
        ];

        $fp = fopen('./themes/'.env('WP_THEME').'/assets/json/store.json', 'w');
        fwrite($fp, json_encode($jsonstore));
        fclose($fp);
    }

    public static function listFilter($url, $apikey){
        $json = file_get_contents($url);
        $listestores  = json_decode($json);
        $listProducts = explode(', ', $listestores->rows[0]->listproducts);
        if(!empty($listProducts[0])) {
            return $listProducts;
        }
        return false;
    }

    public static function copyData() {
        $basepath = realpath(dirname(__FILE__, 6));
        $sourcejs = $basepath.'/vendor/remiinfine/storelocator/src/assets/js/store.js';
        $destjs = $basepath.'/resources/scripts/components/store.js';
        $sourcescss = $basepath.'/vendor/remiinfine/storelocator/src/assets/scss/storelocator.scss';
        $destscss = $basepath.'/resources/styles/components/storelocator.scss';
        copy($sourcejs, $destjs);
        copy($sourcescss, $destscss);
    }
}

?>
