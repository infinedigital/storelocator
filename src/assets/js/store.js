const mapStyle = [{
    'featureType': 'administrative',
    'elementType': 'all',
    'stylers': [{
        'visibility': 'on',
    },
        {
            'lightness': 33,
        },
    ],
},
    {
        'featureType': 'landscape',
        'elementType': 'all',
        'stylers': [{
            'color': '#f5f5f5',
        }],
    },
    {
        'featureType': 'poi.park',
        'elementType': 'geometry',
        'stylers': [{
            'color': '#b6e1b6',
        }],
    },
    {
        'featureType': 'poi.park',
        'elementType': 'labels',
        'stylers': [{
            'visibility': 'on',
        },
            {
                'lightness': 20,
            },
        ],
    },
    {
        'featureType': 'road',
        'elementType': 'all',
        'stylers': [{
            'lightness': 20,
        }],
    },
    {
        'featureType': 'road.highway',
        'elementType': 'geometry',
        'stylers': [{
            'color': 'red',
        }],
    },
    {
        'featureType': 'road.arterial',
        'elementType': 'geometry',
        'stylers': [{
            'color': '#ffffff',
        }],
    },
    {
        'featureType': 'road.local',
        'elementType': 'geometry',
        'stylers': [{
            'color': '#fbfaf7',
        }],
    },
    {
        'featureType': 'water',
        'elementType': 'all',
        'stylers': [{
            'visibility': 'on',
        },
            {
                'color': '#abdaff',
            },
        ],
    },
];
var gmarkers = [];
var dataStore = document.getElementById('jsStore');
var filters = dataStore.dataset.filter;
var country = dataStore.dataset.country;

function initMap() {
    if(country === 'FR') {
        centerCountry = new google.maps.Map(document.getElementById('map'), {
            zoom: 6,
            center: {lat: 47.232193, lng: 2.209667},
            styles: mapStyle,
            mapTypeControl: false
        });
    }else if(country === 'BE') {
        centerCountry = new google.maps.Map(document.getElementById('map'), {
            zoom: 8,
            center: {lat: 50.85045, lng: 4.34878},
            styles: mapStyle,
            mapTypeControl: false
        });
    };

    const map = centerCountry;


    var request = new XMLHttpRequest();
    request.open("GET", "./themes/"+dataStore.dataset.theme+"/assets/json/store.json", false);
    request.overrideMimeType("application/json");
    request.send(null);
    var jsonData = JSON.parse(request.responseText);

    for (var i = 0; i < jsonData.features.length; i++) {
        var coords = jsonData.features[i].geometry.coordinates;
        var latLng = new google.maps.LatLng(coords[1], coords[0]);
        const name = jsonData.features[i]['properties'].name;
        const phone = jsonData.features[i]['properties'].phone;
        const adress = jsonData.features[i]['properties'].adress;
        const position = jsonData.features[i]['geometry'].coordinates;
        const location_place = {lat:parseFloat(position[1]), lng:parseFloat(position[0])};

        let content ='';
        if(phone) {
            content = `
              <div style="margin: 10px;">
                <p class="place">${name}</p>
                <p class="address">${adress}</p>
                <a href="tel:${phone}" class="phone" style="display: block">${phone}</a>
                <a href="" target="_blank">Road to this shop</a>
              </div>
              `;
        }else {
            content = `
              <div style="margin: 10px;">
                <p class="place">${name}</p>
                <p class="address">${adress}</p>
                <a href="" target="_blank">Road to this shop</a>
              </div>
              `;
        }

        const infowindow = new google.maps.InfoWindow({
            content: content,
        });


        //Creating a marker and putting it on the map
        var marker = new google.maps.Marker({
            position: location_place,
            map: map,
            title: "" + jsonData.features[i].properties.mag,
            products: jsonData.features[i].properties.products
        });

        marker.addListener("click", () => {
            infowindow.setContent(content);
            infowindow.setPosition(location_place);
            infowindow.setOptions({pixelOffset: new google.maps.Size(0, -30)});
            infowindow.open(map);
        });
        gmarkers.push(marker);
    }

    const card = document.createElement('div');
    const titleBar = document.createElement('div');
    const container = document.createElement('div');
    const input = document.createElement('input');
    const options = {
        componentRestrictions: {
            country: dataStore.dataset.country
        }
    };

    card.setAttribute('id', 'pac-card');
    container.setAttribute('id', 'pac-container');
    input.setAttribute('id', 'pac-input');
    if(filters === 'false') {
        input.setAttribute('class', 'no-filter');
    }else {
        input.setAttribute('class', 'filter');
    }
    input.setAttribute('type', 'text');
    input.setAttribute('placeholder', 'Enter an address');
    container.appendChild(input);
    card.appendChild(titleBar);
    card.appendChild(container);
    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

    const autocomplete = new google.maps.places.Autocomplete(input, options);

    autocomplete.bindTo('bounds', map);

    autocomplete.setFields(
        ['address_components', 'geometry', 'name']);

    const originMarker = new google.maps.Marker({map: map});

    originMarker.setVisible(false);
    let originLocation = map.getCenter();

    autocomplete.addListener('place_changed', async () => {
        originMarker.setVisible(false);
        originLocation = map.getCenter();
        const place = autocomplete.getPlace();

        if (!place.geometry) {
            window.alert('No address available for input: \'' + place.name + '\'');
            return;
        }

        originLocation = place.geometry.location;
        map.setCenter(originLocation);
        map.setZoom(13);

        originMarker.setPosition(originLocation);
        originMarker.setVisible(true);

        originMarkers.push(originMarker);

        const rankedStores = await calculateDistances(map.data, originLocation);
        showStoresList(map.data, rankedStores);
        return;
    });
}
async function calculateDistances(data, origin) {
    const stores = [];
    const destinations = [];

    data.forEach((store) => {
        const storeNum = store.getProperty('storeid');
        const storeLoc = store.getGeometry().get();

        stores.push(storeNum);
        destinations.push(storeLoc);
    });

    const service = new google.maps.DistanceMatrixService();
    const getDistanceMatrix =
        (service, parameters) => new Promise((resolve, reject) => {
            service.getDistanceMatrix(parameters, (response, status) => {
                if (status != google.maps.DistanceMatrixStatus.OK) {
                    reject(response);
                } else {
                    const distances = [];
                    const results = response.rows[0].elements;
                    for (let j = 0; j < results.length; j++) {
                        const element = results[j];
                        const distanceText = element.distance.text;
                        const distanceDuration = element.duration.text;
                        const distanceVal = element.distance.value;
                        const distanceObject = {
                            storeid: stores[j],
                            distanceText: distanceText,
                            distanceDuration: distanceDuration,
                            distanceVal: distanceVal,
                        };
                        distances.push(distanceObject);
                    }

                    resolve(distances);
                }
            });
        });
    const distancesList = await getDistanceMatrix(service, {
        origins: [origin],
        destinations: destinations,
        travelMode: 'DRIVING',
        unitSystem: google.maps.UnitSystem.METRIC,
    });
    distancesList.sort((first, second) => {
        return first.distanceVal - second.distanceVal;
    });

    return distancesList;
}

// Display list Store List
function showStoresList(data, stores) {
    if (stores.length == 0) {
        console.log('empty stores');
        return;
    }

    let panel = document.createElement('div');
    // If the panel already exists, use it. Else, create it and add to the page.
    if (document.getElementById('panel')) {
        panel = document.getElementById('panel');
        // If panel is already open, close it
        if (panel.classList.contains('open')) {
            panel.classList.remove('open');
        }
    } else {
        panel.setAttribute('id', 'panel');
        const body = document.getElementById('map');
        body.insertBefore(panel, body.childNodes[0]);
    }

    while (panel.lastChild) {
        panel.removeChild(panel.lastChild);
    }

    stores.forEach((store) => {
        const onestore = document.createElement('div');
        onestore.classList.add('onestore');
        const name = document.createElement('p');
        name.classList.add('place');
        const currentStore = data.getFeatureById(store.storeid);
        const address = document.createElement('p');
        address.textContent = currentStore.getProperty('adress');
        name.textContent = currentStore.getProperty('name');
        onestore.appendChild(name);
        onestore.appendChild(address);
        const distanceDuration = document.createElement('p');
        distanceDuration.classList.add('distanceText');
        distanceDuration.textContent = store.distanceDuration;
        onestore.appendChild(distanceDuration);
        const distanceText = document.createElement('p');
        distanceText.classList.add('distanceText');
        distanceText.textContent = store.distanceText;
        onestore.appendChild(distanceText);
        panel.appendChild(onestore);
    });
    panel.classList.add('open');

    return;
}

// Filter based on GoogleSheet Product List
filtersList = JSON.parse(filters);
var filterMarkers = function(category) {
    for (n=0; n<filtersList.length; n++) {
        list = filtersList[n];
        if(category === list) {
            for (i = 0; i < gmarkers.length; i++) {
                marker = gmarkers[i];
                var products = []
                products.push(marker.products);
                for (p = 0; p < products.length; p++) {
                    if (products[p].includes(list)) {
                        let arr = marker.products.split(',');
                        if (arr.includes(list)) {
                            marker.setVisible(true);
                        } else {
                            marker.setVisible(false);
                        }
                    }
                    else {
                        marker.setVisible(false);
                    }
                }
            }
        }
    }
}
